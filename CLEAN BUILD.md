# Clean Build:
```bash
# Init repository and grab Switchroot 18.1 manifest
cd $BUILDBASE/android/lineage
repo init -u https://gitlab.com/photonos/manifest.git -b photon-0.1
repo sync -j${CONNECTIONS}
source build/envsetup.sh

# The steps below must be done in order, otherwise either repo picking or building will fail

# Pick off Gerrit
repopick -t icosa-bt-lineage-17.1
repopick 286019
repopick 270702
repopick 299588

# Patch files, if any of these directories come up as not found make sure to do repo sync again with repo sync -f
git -C bionic am $PATCHDIR/bionic_intrinsics.patch
git -C device/nvidia/t210-common am ${PATCHDIR}/device_nvidia_t210-common-uevent.patch
git -C frameworks/native am ${PATCHDIR}/frameworks_native-surface-debug.patch
git -C system/core am ${PATCHDIR}/system_core-gatekeeper-hack.patch
git -C vendor/maruos/mflinger am ${PATCHDIR}/vendor_maruos_mflinger-debug.patch

# Configure CCache
export USE_CCACHE=1
export CCACHE_EXEC=$(which ccache)
export WITHOUT_CHECK_API=true
ccache -M 50G

lunch photon_icosa_sr-userdebug 
make bacon -j${CONNECTIONS}
```

Download hekate and yeet it's contents on your SD card.

Put https://gitlab.com/ZachyCatGames/q-tips-guide/-/raw/master/res/00-android.ini?inline=false in `/bootloader/ini` on your SD card.

Go to `out/target/product/[device name]` (ex. `out/target/product/photon_icosa_sr`) in your lineage source directory and copy `lineage-17.1-[date]-UNOFFICIAL-[device name].zip` to anywhere on your SD card.

Copy `boot.img` and `install/tegra210-icosa.dtb` from that same directory to `/switchroot/install` on your SD card.

Download https://gitlab.com/ZachyCatGames/q-tips-guide/-/raw/master/res/twrp.img and copy it to `/switchroot/install`.

Launch Hekate and navigate to `Tools` -> `Partition SD Card`.

You can use the `Android ` slider to choose how much storage you want to give to Android, then press `Next Step`, then `Start`.

Once that finishes press `Flash Android`, then `Continue`.

Press `Continue` again and it should reboot to TWRP.

If it doesn't reboot to TWRP, hold the power button for 12 seconds, boot into hekate again, then select the Android config while holding VOL-UP and hold VOL-UP until it gets into TWRP.

In TWRP tap mount, then check any/every partition you can, if some/all cannot be selected, just continue.

Now go back and press `Install` then navigate to `external_sd` then to wherever you put `lineage-17.1-[date]-UNOFFICIAL-[device name].zip` and tap on it.

Swipe to confirm and it should install the rest of Android.

Now just reboot, load hekate again, select the Android config, and hope it boots.
