# Updating:
```bash
# Reset and clean repo, grab new manifest version, update sources
cd $BUILDBASE/android/lineage
repo forall -c 'git reset --hard'
repo forall -c 'git clean -dxf'
git -C .repo/manifests pull
repo sync -j${CONNECTIONS} --force-sync
source build/envsetup.sh

# Pick off Gerrit
repopick -t icosa-bt-lineage-17.1
repopick 286019
repopick 270702
repopick 299588

# Patch files, if any of these directories come up as not found make sure to do repo sync again with repo sync -f
git -C bionic am $PATCHDIR/bionic-intrinsics.patch
git -C device/nvidia/t210-common am ${PATCHDIR}/device_nvidia_t210-common-uevent.patch
git -C frameworks/native am ${PATCHDIR}/frameworks_native-surface-debug.patch
git -C system/core am ${PATCHDIR}/system_core-gatekeeper-hack.patch
git -C vendor/maruos/mflinger am ${PATCHDIR}/vendor_maruos_mflinger-debug.patch

# Configure CCache
export USE_CCACHE=1
export CCACHE_EXEC=$(which ccache)
export WITHOUT_CHECK_API=true
ccache -M 50G

lunch photon_icosa_sr-userdebug 
make bacon -j${CONNECTIONS}
```

Go to `out/target/product/[device name]` (ex. `out/target/product/photon_icosa_sr`) in your lineage source directory and copy `lineage-17.1-[date]-UNOFFICIAL-[device name].zip` to anywhere on your SD card.

If TWRP has been updated since you last flashed, download https://gitlab.com/ZachyCatGames/q-tips-guide/-/raw/master/res/twrp.img and put it in `switchroot/install` on your SD card and in hekate navigate to `Tools` -> `Partition SD Card` then `Flash Android`

Boot into TWRP by holding the volume up button while booting Android, once in TWRP navigate to `external_sd` and flash the zip you placed on your SD card earlier.
